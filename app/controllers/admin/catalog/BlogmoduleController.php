<?php

namespace app\controllers\admin\catalog;

use app\classes\widgets\form\Combo;
use app\classes\widgets\form\Entry;
use app\classes\widgets\form\Form;
use app\classes\widgets\form\Hidden;
use app\classes\widgets\form\Number;
use app\classes\widgets\form\wrapper\FormWrapper;
use app\core\Controller;
use app\models\Banners;
use app\models\Blogconfig;
use app\models\Blogmodule;
use app\models\Module;

class BlogmoduleController extends Controller
{
    private $repository;
    private $blog_config;
    private $module;
    private $usuario;
    private $route = URL_BASE . 'admin-catalog-blogmodule/';

    public function __construct()
    {
        $this->repository   = new Blogmodule;
        $this->module       = new Module;
        $this->banners      = new Banners;
        $this->blog_config  = new Blogconfig;
        $this->usuario      = $this->getSession();
    }

    public function add(string $config_id)
    {
        if (!$config_id) redirect(URL_BASE . 'admin-catalog-blogconfig/');

        $item = $this->blog_config->findById($config_id);
        if (!$item) redirect(URL_BASE . 'admin-catalog-blogconfig/');

        $dados['usuario']           = $this->usuario;
        $dados['breadcrumb'][]      = ['route' => URL_BASE . 'admin-catalog-home', 'title' => 'Painel de controle'];
        $dados['breadcrumb'][]      = ['route' => URL_BASE . 'admin-catalog-blogconfig/', 'title' => 'Blog / Config'];
        $dados['breadcrumb'][]      = ['route' => URL_BASE . 'admin-catalog-blogconfig/edit/' . $config_id, 'title' => 'Editar configuração'];
        $dados['breadcrumb'][]      = ['route' => '#', 'title' => 'Adicionar modulo', 'active' => true];
        $dados['back']              =  URL_BASE . 'admin-catalog-blogconfig/edit/' . $config_id;
        $dados['title']             = 'Novo Slide';
        $dados['topbar']            = $this->load()->controller('admin-common-topbar');
        $dados['sidemenu']          = $this->load()->controller('admin-common-sidemenu', ['admin-catalog-blog']);
        $dados['form']              = $this->form($config_id, 'save', 'Salvar', @getdataform());
        $view                       =  'adm/pages/catalog/modules/default/add'; //é uma view padrão, serve para renderizar forms, podes usar sempre coisa :)
        $this->renderView($view, $dados);
    }

    public function edit(string $config_id, string $idmodule)
    {
        if (!$config_id) redirect(URL_BASE . 'admin-catalog-blogconfig/');

        $item = $this->blog_config->findById($config_id);
        if (!$item) redirect(URL_BASE . 'admin-catalog-blogconfig/');

        $item = $this->repository->findById($idmodule);
        if (!$item) redirect(URL_BASE . 'admin-catalog-blogconfig/');

        $dados['usuario']           = $this->usuario;
        $dados['breadcrumb'][]      = ['route' => URL_BASE . 'admin-catalog-home', 'title' => 'Painel de controle'];
        $dados['breadcrumb'][]      = ['route' => URL_BASE . 'admin-catalog-modules', 'title' => 'Modulos'];
        $dados['breadcrumb'][]      = ['route' => URL_BASE . 'admin-catalog-blogconfig/edit/' . $config_id, 'title' => 'Blog / Config'];
        $dados['breadcrumb'][]      = ['route' => '#', 'title' => 'Editar modulo', 'active' => true];
        $dados['back']              =  URL_BASE . 'admin-catalog-blogconfig/edit/' . $config_id;
        $dados['title']             = 'Novo Slide';
        $dados['topbar']            = $this->load()->controller('admin-common-topbar');
        $dados['sidemenu']          = $this->load()->controller('admin-common-sidemenu', ['admin-catalog-blog']);
        $dados['form']              = $this->form($config_id, 'update', 'Editar', @getdataform() ?: $item);
        $view                       =  'adm/pages/catalog/modules/default/add';
        $this->renderView($view, $dados);
    }

    public function remove(string $config_id, string $idmodule)
    {
        if (!$config_id) redirect(URL_BASE . 'admin-catalog-blogconfig/');

        $item = $this->blog_config->findById($config_id);
        if (!$item) redirect(URL_BASE . 'admin-catalog-blogconfig/');

        $item = $this->repository->findById($idmodule);
        if (!$item) redirect(URL_BASE . 'admin-catalog-blogconfig/');

        $dados['usuario']           = $this->usuario;
        $dados['breadcrumb'][]      = ['route' => URL_BASE . 'admin-catalog-home', 'title' => 'Painel de controle'];
        $dados['breadcrumb'][]      = ['route' => URL_BASE . 'admin-catalog-modules', 'title' => 'Modulos'];
        $dados['breadcrumb'][]      = ['route' => URL_BASE . 'admin-catalog-blogconfig/edit/' . $config_id, 'title' => 'Blog / Config'];
        $dados['breadcrumb'][]      = ['route' => '#', 'title' => 'Remover modulo', 'active' => true];
        $dados['title']             = 'Remover modulo';
        $dados["toptitle"]          = 'Remover modulo';
        $dados['back']              =  URL_BASE . 'admin-catalog-blogconfig/edit/' . $config_id;
        $dados['form']              = $this->form($config_id, 'delete', 'Remover', @getdataform() ?: $item);
        $dados['topbar']            = $this->load()->controller('admin-common-topbar');
        $dados['sidemenu']          = $this->load()->controller('admin-common-sidemenu', ['admin-catalog-blog']);
        $view                       = "adm/pages/catalog/layout/layout/add";
        $this->renderView($view, $dados);
    }

    public function save(string $config_id)
    {
        if (!$config_id) redirect(URL_BASE . 'admin-catalog-blogconfig/');

        $page = $this->blog_config->findById($config_id);
        if (!$page) redirect(URL_BASE . 'admin-catalog-blogconfig/');

        $request                    = $_POST;
        $request                    = filterpost($request);

        if (!$request['module_id'] || !$request['position']) {
            setdataform($request);
            setmessage(['tipo' => 'warning', 'msg' => 'Os campos com (*) são de preenchimento obrigatório!']);
            redirectBack($this->route . 'add/' . $config_id);
        }

        $blog_module                        = $this->repository;
        $blog_module->blog_config_id        = $request['blog_config_id'];
        $blog_module->module_id             = $request['module_id'];
        $blog_module->position              = $request['position'] ?: 'T';
        $blog_module->show_in_article       = $request['show_in_article'] ?: 'S';
        $blog_module->sort_order            = $request['sort_order'] ?: '0';
        $blog_module->enable                = $request['enable'] ?: 'S';
        $blog_moduleId                      = $blog_module->save();
        if ($blog_moduleId) {
            setmessage(['tipo' => 'success', 'msg' => 'Modulo adicionado com sucesso!']);
            redirect(URL_BASE . 'admin-catalog-blogconfig/edit/' . $config_id);
        }
    }

    public function update(string $config_id, string $id_module)
    {
        if (!$config_id || !$id_module) redirect(URL_BASE . 'admin-catalog-blogconfig/');

        $item = $this->repository->findById($id_module);
        if (!$item) redirect(URL_BASE . 'admin-catalog-blogconfig/');

        $request                    = $_POST;
        $request                    = filterpost($request);

        if (!$request['module_id'] || !$request['position']) {
            $request['id'] = $id_module;
            setdataform($request);
            setmessage(['tipo' => 'warning', 'msg' => 'Os campos com (*) são de preenchimento obrigatório!']);
            redirect($this->route . 'edit/' . $config_id . '/' . $id_module);
        }

        $item->blog_config_id       = $request['blog_config_id'];
        $item->module_id            = $request['module_id'];
        $item->position             = $request['position'] ?: 'T';
        $item->show_in_article      = $request['show_in_article'] ?: 'S';
        $item->sort_order           = $request['sort_order'] ?: '0';
        $item->enable               = $request['enable'] ?: 'S';
        $itemId                     = $item->save();
        if ($itemId) {
            setmessage(['tipo' => 'success', 'msg' => 'Modulo editado com sucesso']);
            redirect(URL_BASE . 'admin-catalog-blogconfig/edit/' . $config_id);
        } else {
            setmessage(['tipo' => 'warning', 'msg' => 'Ocorreu um erro na requisição']);
            redirect(URL_BASE . 'admin-catalog-blogconfig/edit/' . $config_id);
        }
    }

    public function delete(string $config_id, string $id_module)
    {
        if (!$config_id || !$id_module) redirectBack();

        $item = $this->repository->findById($id_module);
        if (!$item) redirect(URL_BASE . 'admin-catalog-blogconfig/');

        $itemId = $item->destroy();
        if ($itemId) {
            setmessage(['tipo' => 'success', 'msg' => 'Modulo removido com sucesso']);
            redirect(URL_BASE . 'admin-catalog-blogconfig/edit/' . $config_id);
        } else {
            setmessage(['tipo' => 'warning', 'msg' => 'Ocorreu um erro na requisição']);
            redirect(URL_BASE . 'admin-catalog-blogconfig/edit/' . $config_id);
        }
    }

    private function form(string $idconfig, string $action = 'save', string $buttonlabel = "Salvar", $data = null)
    {
        $readyonly = $action == 'delete' ? false : true;

        $form = new FormWrapper(new Form('form_moduleslayout'));
        $module_id = new Combo('module_id', $readyonly);
        $module_id->addItems($this->module->getmodules());
        $position = new Combo('position', $readyonly);
        $position->addItems([
            'n' => 'Selecione uma opção',
            'T' => 'Topo',
            'F' => 'Rodapé',
        ]);

        $show_in_article = new Combo('show_in_article', $readyonly);
        $show_in_article->addItems([
            'S' => 'Sim',
            'N' => 'Não',
        ]);
        $sort_order = new Number('sort_order', $readyonly);
        $enable = new Combo('enable', $readyonly);
        $enable->addItems(['S' => 'Ativo', 'N' => 'Inativo']);
        $blog_config_id = new Hidden('blog_config_id');
        $blog_config_id->setProperty('value', $idconfig);

        $form->addField($module_id,     ['label' => 'Modulo *', 'css' => 'mb-4']);
        $form->addField($position,      ['label' => 'Posição *', 'css' => 'mb-4']);
        $form->addField($show_in_article,      ['label' => 'Mostrar esse modulo nos artigos?', 'css' => 'mb-4']);
        $form->addField($sort_order,    ['label' => 'Ordem', 'css' => 'mb-4']);
        $form->addField($enable,        ['label' => 'Situação', 'css' => 'mb-4']);
        $form->addField($blog_config_id, []);

        if ($data) {
            $form->addAction($buttonlabel, (object)['submit' => true, 'css' => 'btn btn-success', 'route' => $this->route . $action . '/' . $idconfig . '/' . @$data->id]);
            $form->setData($data);
        } else {
            $form->addAction($buttonlabel, (object)['submit' => true, 'css' => 'btn btn-success', 'route' => $this->route . $action . '/' . $idconfig]);
        }
        return  $form->getForm();
    }
}
