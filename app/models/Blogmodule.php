<?php

namespace app\models;

use CoffeeCode\DataLayer\DataLayer;

class Blogmodule extends DataLayer
{
    public function __construct()
    {
        parent::__construct('blog_module', ['blog_config_id', 'module_id', 'position','sort_order', 'enable'], 'id', false);
    }

    public function list($blog_config_id)
    {
        $module = new Module;
        $list = $this->find("blog_config_id=:blog_config_id", "blog_config_id={$blog_config_id}")->order("sort_order ASC")->fetch(true);
        if ($list) {
            foreach ($list as $key => $item) {
                $m = $module->findById($item->module_id);
                $item->description = $m->description;
                $list[$key] = $item;
            }
        }
        return $list;
    }
}
